package sk.sintez.fft.ui

import android.os.Bundle
import android.view.*
import androidx.fragment.app.Fragment
import sk.sintez.fft.R
import sk.sintez.fft.databinding.FragmentMainBinding
import sk.sintez.fft.viewmodel.MainViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel
import sk.sintez.fft.viewmodel.AudioParams


class MainFragment : Fragment() {

    companion object {
        fun newInstance() = MainFragment()
    }

    private lateinit var binding: FragmentMainBinding
    private val viewModel by viewModel<MainViewModel>()

    private var audioProcessingParams = AudioParams()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.menu, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem) = when (item.itemId) {
        R.id.setup1 -> {
            viewModel.stop()
            audioProcessingParams = AudioParams(
                sampleRateHz = 44100,
                fftLen = 2048
            )
            activity?.title = "FFT 2048"
            true
        }
        R.id.setup2 -> {
            viewModel.stop()
            audioProcessingParams = AudioParams(
                sampleRateHz = 44100,
                fftLen = 4096
            )
            activity?.title = "FFT 4096"
            true
        }
        else -> super.onOptionsItemSelected(item)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View {
        binding = FragmentMainBinding.inflate(inflater, container, false).apply {
            viewModel.mutableLiveData.observe(viewLifecycleOwner) {
                val resId = if (it)
                    R.string.stop_button_text
                else
                    R.string.start_button_text

                buttonStart.setText(resId)
            }

//            viewModel.waveFullLiveData.observe(viewLifecycleOwner) {
//                waveView2.setData(it)
//            }

            viewModel.waveLiveData.observe(viewLifecycleOwner) {
                waveView.setData(it)
            }

            viewModel.fftLiveData.observe(viewLifecycleOwner) {
                fftView.setData(it)
            }

            viewModel.metricsLiveData.observe(viewLifecycleOwner) {
                timingTextView.text = it
            }

            buttonStart.setOnClickListener {
                if (viewModel.mutableLiveData.value == true)
                    viewModel.stop()
                else
                    viewModel.start(audioProcessingParams)
            }

            buttonStart.isEnabled = true
        }

        activity?.title = "FFT 2048"

        return binding.root;
    }

}