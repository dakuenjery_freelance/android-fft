package sk.sintez.fft.ui

import android.content.Context
import android.graphics.Canvas
import android.graphics.Paint
import android.graphics.Path
import android.util.AttributeSet
import android.view.View
import android.widget.ProgressBar
import androidx.annotation.ColorInt


interface Renderer {
    fun updateData(data: FloatArray)
    fun render(canvas: Canvas)
}

class RenderableView : View {
    private var renderer: Renderer = EmptyRenderer()

    constructor(context: Context?) : super(context) {}
    constructor(context: Context?, attrs: AttributeSet?) : super(context, attrs) {}
    constructor(context: Context?, attrs: AttributeSet?, defStyleAttr: Int) : super(
        context,
        attrs,
        defStyleAttr
    ) {
    }

    fun setRenderer(renderer: Renderer) {
        this.renderer = renderer
    }

    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)
        renderer.render(canvas)
    }
}

class EmptyRenderer : Renderer {
    override fun updateData(data: FloatArray) { }
    override fun render(canvas: Canvas) { }
}


internal class SimpleWaveformRenderer(
    @field:ColorInt @param:ColorInt private val backgroundColour: Int,
    private val foregroundPaint: Paint,
    private val waveformPath: Path,
    private val view: RenderableView
) :
    Renderer
{
    private var data: FloatArray? = null

    override fun updateData(data: FloatArray) {
//        this.data = data.copyOf(data.size)
        this.data = data
        view.invalidate()
    }

    override fun render(canvas: Canvas) {
        canvas.drawColor(backgroundColour)

        val width = canvas.width.toFloat()
        val height = canvas.height.toFloat()
        waveformPath.reset()

        if (data != null) {
            renderWaveform(data!!, width, height)
        } else {
            renderBlank(width, height)
        }

        canvas.drawPath(waveformPath, foregroundPaint)
    }

    private fun renderWaveform(waveform: FloatArray, width: Float, height: Float) {
        var maxY = 0f

        val xIncrement = width / waveform.size.toFloat()
        val yIncrement = height / Y_FACTOR
        val halfHeight = (height * HALF_FACTOR).toInt()
        waveformPath.moveTo(0f, halfHeight.toFloat())

        var y = height - yIncrement * waveform[1]
        waveformPath.moveTo(0f, y)

        for (i in 1 until waveform.size) {
//            val yPosition = if (waveform[i] > 0)
//                                height - yIncrement * waveform[i]
//                            else
//                                -(yIncrement * waveform[i])

//            waveformPath.lineTo(xIncrement * i, yPosition)

            y = height - yIncrement * waveform[i]
            waveformPath.lineTo(xIncrement * i, y)

            maxY = Math.max(maxY, waveform[i])
        }

        Y_FACTOR = (maxY * 1.1).toInt()
    }

    private fun renderBlank(width: Float, height: Float) {
        val y = (height * HALF_FACTOR).toInt()
        waveformPath.moveTo(0f, y.toFloat())
        waveformPath.lineTo(width, y.toFloat())
    }

    private var Y_FACTOR = 10

    companion object {
        private const val HALF_FACTOR = 0.5f

        fun newInstance(
            @ColorInt backgroundColor: Int,
            @ColorInt foregroundColor: Int,
            view: RenderableView
        ): SimpleWaveformRenderer {
            val paint = Paint()
            paint.color = foregroundColor
            paint.isAntiAlias = true
            paint.style = Paint.Style.STROKE
            val waveformPath = Path()

            return SimpleWaveformRenderer(backgroundColor, paint, waveformPath, view).apply {
                view.setRenderer(this)
            }
        }
    }
}

class SimpleProgressRenderer(val progressBar: ProgressBar) : Renderer {
    override fun updateData(data: FloatArray) {
        val ampl = (data[0] * 100).toInt()
        progressBar.progress = ampl
        progressBar.invalidate()

//        println("Ampl: $ampl")
    }

    override fun render(canvas: Canvas) {

    }

}