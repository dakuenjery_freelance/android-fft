package sk.sintez.fft.view

import android.content.Context
import android.graphics.*
import android.util.AttributeSet
import android.view.View
import androidx.core.graphics.minus
import androidx.core.graphics.plus
import org.koin.core.component.KoinApiExtension
import org.koin.core.component.KoinComponent
import org.koin.core.component.inject
import sk.sintez.fft.module.AudioDataArray
import sk.sintez.fft.module.IPerfMetrics
import kotlin.math.*


interface IRender {
    fun setData(data: AudioRenderData)
}


class AudioRenderData(val data: AudioDataArray, val sampleRateHz: Int)

abstract class AbstractRenderPathView @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : View(context, attrs, defStyleAttr), IRender, KoinComponent {

    protected enum class AxeDirection {
        HORIZONTAL, VERTICAL
    }

    protected data class DrawAxeParams (
        val textPoint: PointF,
        val signLineStart: PointF,
        val signLineEnd: PointF,
        val signPointOffset: PointF,
        val axeStart: PointF,
        val axeEnd: PointF,
        val dotValues: List<String>
    )

    private val RENDER_MEASURE_TICK_ID: String by lazy {
        val tickId = "render ${tag}"
        perfMetric.prepare(arrayListOf(tickId))
        tickId
    }

    private val perfMetric by inject<IPerfMetrics>()

    private var data = AudioRenderData(AudioDataArray(0), 0)

    private val backgroundColor = Color.BLACK
    private val foregroundColor = Color.WHITE

    private val pathPaint = Paint().apply {
        color = foregroundColor
        isAntiAlias = true
        style = Paint.Style.STROKE
        strokeWidth = 1f // !!
    }

    private val textPaint = Paint().apply {
        color = foregroundColor
        isAntiAlias = true
        style = Paint.Style.FILL_AND_STROKE
        textSize = 18f
        textAlign = Paint.Align.RIGHT
    }

    private val path = Path()

    override fun onDraw(canvas: Canvas) {
        perfMetric.measure(RENDER_MEASURE_TICK_ID) {
            super.onDraw(canvas)
            val renderData = this.data

            canvas.drawColor(backgroundColor)

            if (renderData.data.isNotEmpty()) {
                path.reset()
                drawData(canvas, renderData)
            }
        }
    }

    private fun drawData(canvas: Canvas, renderData: AudioRenderData) {
        val data = renderData.data

        val axesSizes = getAxesSizes()
        val chartOffset = PointF(axesSizes.y, axesSizes.x)

        if (chartOffset.x > 0)
            chartOffset.x += 20f

        if (chartOffset.y > 0)
            chartOffset.y += 20f

        val fullCanvasSize = PointF(canvas.width.toFloat(), canvas.height.toFloat())
        val canvasSize = fullCanvasSize - chartOffset - PointF(0f, textPaint.textSize)

        val axesDots = getAxesDotCount()

        val drawPointOffset = getZeroOffset(canvasSize) + PointF(chartOffset.x, 0f)

        if (axesDots.x <= 0)
            drawPointOffset.y += textPaint.textSize/2
        else
            drawPointOffset.y += textPaint.textSize

        val minMaxPoint = getMinMaxPoint()

        val pointStep = canvasSize / PointF(data.size.toFloat(), -(abs(minMaxPoint.y - minMaxPoint.x)))

        beginPaint(canvasSize, renderData)

        var point = getDrawPoint(0f, data[0])
        point = point * pointStep + drawPointOffset
        path.moveTo(point.x, point.y)

        for (i in 1 until data.size) {
            point = getDrawPoint(i.toFloat(), data[i])
            point = point * pointStep + drawPointOffset
            path.lineTo(point.x, point.y)
        }

        canvas.drawPath(path, pathPaint)

        if (axesDots.x > 0) {
            val r = RectF(chartOffset.x, fullCanvasSize.y - axesSizes.x, 0f, 0f)
            val p = getHorizontalAxeParams(PointF(0f, data.size.toFloat()), fullCanvasSize, r, axesDots.x)
            drawAxe(canvas, p)
        }

        if (axesDots.y > 0) {
            val r = RectF(0f, textPaint.textSize/2, fullCanvasSize.x - axesSizes.y, chartOffset.y)

            if (axesDots.x <= 0)
                r.bottom += textPaint.textSize/2

            val p = getVerticalAxeParams(minMaxPoint, fullCanvasSize, r, axesDots.y)
            drawAxe(canvas, p)
        }

        endPaint()
    }

    protected fun drawAxe(canvas: Canvas, axeParams: DrawAxeParams) {
        val (textPoint, signLineStart, signLineEnd, signPointOffset, axeStart, axeEnd, dots) = axeParams

        for (signValue in dots) {
            canvas.drawLine(signLineStart.x, signLineStart.y, signLineEnd.x, signLineEnd.y, pathPaint)
            canvas.drawText(signValue, textPoint.x, textPoint.y, textPaint)

            signLineStart.offset(signPointOffset.x, signPointOffset.y)
            signLineEnd.offset(signPointOffset.x, signPointOffset.y)
            textPoint.offset(signPointOffset.x, signPointOffset.y)
        }

        canvas.drawLine(axeStart.x, axeStart.y, axeEnd.x, axeEnd.y, pathPaint)
    }

    protected open fun getHorizontalAxeParams(minMaxValue: PointF, canvasSize: PointF, viewRect: RectF, dotCount: Int): DrawAxeParams {
        val width = canvasSize.x - viewRect.left - viewRect.right
        val height = canvasSize.y - viewRect.top - viewRect.bottom

        val offset = PointF(viewRect.left, viewRect.top)

        val textPoint = PointF(offset.x, height * 0.25f + textPaint.textSize + offset.y)

        val signLineStart = PointF(0f, height * 0f) + offset
        val signLineEnd = PointF(0f, height * 0.2f) + offset

        val signPointOffset = PointF(width/dotCount, 0f)

        val axeStart = PointF(0f, height * 0.1f) + offset
        val axeEnd = PointF(width, height * 0.1f) + offset

        val dots = axeDots(minMaxValue, dotCount, AxeDirection.HORIZONTAL)

        return DrawAxeParams(
            textPoint, signLineStart, signLineEnd, signPointOffset, axeStart, axeEnd, dots
        )
    }

    protected open fun getVerticalAxeParams(minMaxValue: PointF, canvasSize: PointF, viewRect: RectF, dotCount: Int): DrawAxeParams {
        val width = canvasSize.x - viewRect.left - viewRect.right
        val height = canvasSize.y - viewRect.top - viewRect.bottom

        val offset = PointF(viewRect.left, viewRect.top)

        val textPoint = PointF(width * 0.75f + offset.x, textPaint.textSize/2 + offset.y/2 - 1)

        val signLineStart = PointF(width * 0.8f, 0f) + offset
        val signLineEnd = PointF(width * 1f, 0f) + offset

        val signPointOffset = PointF(0f, height/dotCount)

        val axeStart = PointF(width * 0.9f, 0f) + offset
        val axeEnd = PointF(width * 0.9f, height) + offset

        val dots = axeDots(minMaxValue, dotCount, AxeDirection.VERTICAL)

        return DrawAxeParams(
            textPoint, signLineStart, signLineEnd, signPointOffset, axeStart, axeEnd, dots
        )
    }

    abstract fun getZeroOffset(canvasSize: PointF): PointF
    abstract fun getMinMaxPoint(): PointF

    abstract fun getDrawPoint(x: Float, y: Float): PointF

    abstract fun getAxesDotCount(): Point
    abstract fun getAxesSizes(): PointF

    protected open fun getAxeStepValue(axeStep: Float, minMaxValue: PointF, stepIndex: Int, direction: AxeDirection): String {
        val value = (minMaxValue.y - axeStep * stepIndex).toInt()
        return "$value"
    }

    abstract fun beginPaint(canvasSize: PointF, renderData: AudioRenderData)
    abstract fun endPaint()

    protected open fun axeDots(minMaxValue: PointF, dotCount: Int, direction: AxeDirection): List<String> {
        val axeStep = (Math.abs(minMaxValue.x) + Math.abs(minMaxValue.y)) / dotCount

        val signSteps = (0..dotCount).map {
            getAxeStepValue(axeStep, minMaxValue, it, direction)
        }

        return signSteps
    }

    override fun setData(data: AudioRenderData) {
        this.data = data
        invalidate()
    }
}

private operator fun PointF.div(point: PointF) = PointF(this.x / point.x, this.y / point.y)
private operator fun PointF.times(point: PointF) = PointF(this.x * point.x, this.y * point.y)

private fun formatAxeSign(thousandsSign: Array<String>, markPositive: Boolean, commaNumbers: Int = -1, f: () -> Float): String {
    val value = f()

    val thousands = if (abs(value) > 1000)
        min((log(abs(value), 10f)/3).toInt(), thousandsSign.size-1)
    else
        0

    val d = 1000f.pow(thousands)
    val k = thousandsSign[thousands]

    val normValue = value / d

    val r = when {
        commaNumbers > 0 -> commaNumbers
        abs(value) < 1000 || abs(value) > 9999 -> 0
        else -> 1
    }

    val vstr = String.format("%.${r}f", normValue)
    val s = if (markPositive && value > 0) "+" else ""

    return "$s$vstr$k"
}

@KoinApiExtension
open class RenderPathView @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : AbstractRenderPathView(context, attrs, defStyleAttr)
{
    protected val karr = arrayOf("", "k", "m")

    private var yMaxValueTmp = 10f
    private var yMaxValue = 10f

    override fun getZeroOffset(canvasSize: PointF) = PointF(0f, canvasSize.y/2)

    override fun getMinMaxPoint() = PointF(-yMaxValue, yMaxValue)

    override fun getDrawPoint(x: Float, y: Float): PointF {
        yMaxValueTmp = yMaxValueTmp.coerceAtLeast(abs(y))
        return PointF(x, y)
    }

    override fun getAxeStepValue(axeStep: Float, minMaxValue: PointF, stepIndex: Int, direction: AxeDirection): String {
        return formatAxeSign(karr, true) {
            minMaxValue.y - axeStep * stepIndex
        }
    }

    override fun getAxesDotCount(): Point = Point(0, 6)

    override fun getAxesSizes() = PointF(0f, 80f)

    override fun beginPaint(canvasSize: PointF, renderData: AudioRenderData) {
        yMaxValueTmp = 10f
    }

    override fun endPaint() {
        yMaxValue = yMaxValueTmp
    }
}

@KoinApiExtension
class RenderFftView @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : RenderPathView(context, attrs, defStyleAttr) {
    private val hzArr = arrayOf("hz", "kHz", "mHz")

    private var sampleRateHz: Float = 1f
    private var yMaxValueTmp = 10f
    private var yMaxValue = 10f

    override fun getZeroOffset(canvasSize: PointF) = PointF(0f, canvasSize.y)

    override fun getMinMaxPoint() = PointF(0f, yMaxValue)

    override fun getDrawPoint(x: Float, y: Float): PointF {
        yMaxValueTmp = yMaxValueTmp.coerceAtLeast(y)
//        val logX = log(x, 10f)
//        val logX1 = logX * (2048f/log(2048f, 10f))
//
//        val xValue = if (x < 0.0001) 0f else logX1

        return PointF(x, y)
//        return PointF(xValue, y)
    }

    override fun getAxesDotCount(): Point = Point(8, 8)
    override fun getAxesSizes() = PointF(50f, 80f)

    override fun getAxeStepValue(
        axeStep: Float,
        minMaxValue: PointF,
        stepIndex: Int,
        direction: AxeDirection
    ): String {
        return if (direction == AxeDirection.VERTICAL) {
            formatAxeSign(karr, false) {
                minMaxValue.y - axeStep * stepIndex
            }
        } else {
            formatAxeSign(hzArr, false, 1) {
                val dots = getAxesDotCount().x
                val peakIndex = (minMaxValue.y * (stepIndex.toFloat() / dots)).toInt()
                sampleRateHz * peakIndex / minMaxValue.y / 2
            }
        }
    }

    override fun beginPaint(canvasSize: PointF, renderData: AudioRenderData) {
        sampleRateHz = renderData.sampleRateHz.toFloat()
        yMaxValueTmp = 10f
    }

    override fun endPaint() {
        yMaxValue = yMaxValueTmp
    }
}
