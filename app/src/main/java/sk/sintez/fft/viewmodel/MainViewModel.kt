package sk.sintez.fft.viewmodel

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import sk.sintez.fft.module.AudioDataArray
import sk.sintez.fft.module.IAudioListener
import sk.sintez.fft.module.IPerfMetrics
import sk.sintez.fft.module.PerfMetricsFormatter
import sk.sintez.fft.view.AudioRenderData
import kotlin.math.sqrt

private const val ON_DATA_RECEIVED_TICK_MEASURE_ID = "capture"
private const val ON_DATA_PROCESSING_MEASURE_ID = "processing"


data class AudioParams(
    val sampleRateHz: Int = 44100,
    val bufSize: Int = 1024,

    val waveLen: Int = sampleRateHz * 3,
    val fftLen: Int = 2048
)

class MainViewModel(private val audioListener: IAudioListener, private val perfMetric: IPerfMetrics) : ViewModel(), IAudioListener.Callback {
    val waveLiveData = MutableLiveData<AudioRenderData>()
    val fftLiveData = MutableLiveData<AudioRenderData>()
    val mutableLiveData = MutableLiveData<Boolean>(false)
    val metricsLiveData = MutableLiveData<String>()

    private lateinit var metricsFormatter: PerfMetricsFormatter

    private var processing: AudioProcessing? = null

    fun start(params: AudioParams) {
        stop()

        metricsFormatter = PerfMetricsFormatter("16bit/${params.sampleRateHz.toFloat()/1000}kHz ${params.bufSize}")

        perfMetric.prepare(arrayListOf(ON_DATA_RECEIVED_TICK_MEASURE_ID, ON_DATA_PROCESSING_MEASURE_ID))

        processing = AudioProcessing(params)

        audioListener.start(IAudioListener.Params(params.sampleRateHz, params.bufSize), this)
        mutableLiveData.postValue(true)
    }

    fun stop() {
        processing?.let {
            audioListener.stop()
            mutableLiveData.postValue(false)

            this.processing = null

            waveLiveData.postValue(AudioRenderData(AudioDataArray(1, { 0f }), 1))
            fftLiveData.postValue(AudioRenderData(AudioDataArray(1, { 0f }), 1))

            Thread.sleep(10L)
        }
    }

    override fun onDataReceived(data: AudioDataArray) {
        perfMetric.tick(ON_DATA_RECEIVED_TICK_MEASURE_ID)

        perfMetric.measure(ON_DATA_PROCESSING_MEASURE_ID) {
            processing?.process(data)
        }

        val m = metricsFormatter.format(perfMetric)
        metricsLiveData.postValue(m)
    }


    private inner class AudioProcessing(val params: AudioParams) {
        private val waveCircularBuffer = CircularMultiBuffer(params.waveLen, 1)
        private val desampleWaveCircularBuffer = DownsampledCircularMultiBuffer(params.waveLen, 20, 1)

        private val fftBuffer = AudioDataArray(params.fftLen*2)
        private val fftResultData = AudioDataArray(params.fftLen)

        init {
            if (params.waveLen < params.fftLen)
                throw IllegalArgumentException("waveLen < fftLen")
        }

        private val fft = org.jtransforms.fft.FloatFFT_1D(params.fftLen.toLong() * 2)

//        private val fft = org.jtransforms.fft.FloatFFT_1D(4096 * 2)

        fun process(buf: AudioDataArray) {
            val waveData = waveCircularBuffer.append(buf)

            val fftResultData = getFft(waveData)
            val waveResultData = desampleWaveCircularBuffer.append(buf)

            waveLiveData.postValue(AudioRenderData(waveResultData, params.sampleRateHz))
            fftLiveData.postValue(AudioRenderData(fftResultData, params.sampleRateHz))
        }

        private fun getFft(waveData: AudioDataArray): AudioDataArray {
            // преобразование фурье выполняется в том же массиве и портит его. Нужна копия
            // Берем последние значения из waveData
            val offset = waveData.size - fftBuffer.size
            for (i in 0 until fftBuffer.size)
                fftBuffer[i] = waveData[i + offset]

            try {
                fft.realForward(fftBuffer)
            } catch (e: InterruptedException) {
                Log.e("FFT", e.toString())
            } catch (e: Exception) {
                Log.e("FFT", e.toString())
            }

            for (i in 0 until fftResultData.size) {
                val re = fftBuffer[2*i]
                val im = fftBuffer[2*i + 1]
                val magnitude = sqrt((re*re + im*im).toDouble())
                fftResultData[i] = magnitude.toFloat()
            }

            return fftResultData
        }
    }
}

open class CircularMultiBuffer(val bufSize: Int, bufCount: Int) {
    protected val bufArr = Array(bufCount) { AudioDataArray(bufSize) }
    protected var bufInd = 0

    open fun append(input: AudioDataArray, offset: Int = 0, length: Int = input.size): AudioDataArray {
        val buf1 = bufArr[bufInd]
        bufInd = (bufInd + 1) % bufArr.size
        val buf2 = bufArr[bufInd]

        buf1.copyInto(buf2, destinationOffset = 0, startIndex = length)
        input.copyInto(buf2, destinationOffset = buf2.size - length, startIndex = offset, endIndex = offset + length)

        return buf2
    }
}

class DownsampledCircularMultiBuffer(fullBufSize: Int, val desampleRate: Int = 10, bufCount: Int = 2) : CircularMultiBuffer(fullBufSize/desampleRate, bufCount) {
    private val tmpBuffer = AudioDataArray(bufSize)

    override fun append(input: AudioDataArray, offset: Int, length: Int): AudioDataArray {
        val downsampledInput = downsampleWave(input)
        return super.append(downsampledInput, offset, input.size / desampleRate)
    }

    private fun downsampleWave(waveData: AudioDataArray): AudioDataArray {
        val n = desampleRate

        for (i in 0 until waveData.size / n) {
            val ni = n * i
            var value = waveData[ni]

            for (j in ni until ni+n)
                if (Math.abs(waveData[j]) > Math.abs(value))
                    value = waveData[j]

            tmpBuffer[i] = value
        }

        return tmpBuffer
    }
}

