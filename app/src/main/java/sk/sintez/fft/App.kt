package sk.sintez.fft

import android.app.Application
import org.koin.android.ext.koin.androidContext
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.core.context.startKoin
import org.koin.dsl.module
import sk.sintez.fft.module.AudioListenerImpl
import sk.sintez.fft.module.IAudioListener
import sk.sintez.fft.module.IPerfMetrics
import sk.sintez.fft.module.PerfMetricsImpl
import sk.sintez.fft.viewmodel.MainViewModel
import java.util.*


val coreModule = module {
    single<IAudioListener> { AudioListenerImpl(get()) }
    single<IPerfMetrics> { PerfMetricsImpl(10) }

    viewModel { MainViewModel(get(), get()) }
}

class App : Application() {
    override fun onCreate(){
        super.onCreate()

        startKoin {
            androidContext(this@App)
            modules(coreModule)
        }
    }

    override fun onTerminate() {
        super.onTerminate()
    }
}