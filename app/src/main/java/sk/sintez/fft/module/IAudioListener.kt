package sk.sintez.fft.module

import android.content.Context
import android.media.AudioFormat
import android.media.AudioRecord
import android.media.MediaRecorder
import android.util.Log

interface IAudioListener {

    interface Callback {
        fun onDataReceived(data: AudioDataArray)
    }

    data class Params(
        val sampleRateHz: Int,
        val bufSize: Int,
//        val audioChannel: Int = AudioFormat.CHANNEL_IN_MONO,
//        val audioFormat: Int = AudioFormat.ENCODING_PCM_16BIT
    )

    fun start(params: Params, callback: Callback)
    fun stop()
}

typealias AudioDataArray = FloatArray

class AudioListenerImpl(context: Context) : IAudioListener {
    private var thread: Thread? = null

    override fun start(params: IAudioListener.Params, callback: IAudioListener.Callback) {
        if (this.thread != null)
            stop()

        val minBufSize = AudioRecord.getMinBufferSize(
            params.sampleRateHz,
            AudioFormat.CHANNEL_IN_MONO,
            AudioFormat.ENCODING_PCM_16BIT
        )

        if (minBufSize < params.bufSize)
            println("WARN: minBufSize < params.bufSize. {minBufSize} < {params.bufSize}")

        val audioRec = AudioRecord(
            MediaRecorder.AudioSource.MIC,
            params.sampleRateHz,
            AudioFormat.CHANNEL_IN_MONO,
            AudioFormat.ENCODING_PCM_16BIT,
            params.bufSize
        )

        thread = CaptureThread(params, audioRec, callback).also {
            it.setUncaughtExceptionHandler { t, e ->
                Log.e("FFT", e.toString())
            }
            it.start()
        }
    }

    override fun stop() {
        thread?.let {
            try {
                it.interrupt()
                it.join()
            } catch (e: InterruptedException) {
                Log.e("FFT", e.toString())
            }
        }

        thread = null
    }
}

class CaptureThread(params: IAudioListener.Params, private val audioRec: AudioRecord, private val callback: IAudioListener.Callback) : Thread("Audio capture thread") {
    private val buf = ShortArray(params.bufSize)
    private val audioData = AudioDataArray(params.bufSize)
    private var bufOffset = 0

//    private val fft = org.jtransforms.fft.FloatFFT_1D(bufSize.toLong())

    override fun run() {
        try {
            audioRec.startRecording()

            sleep(10);

            while (!isInterrupted) {
                val readed = audioRec.read(buf, bufOffset, buf.size)
                bufOffset += readed

                if (bufOffset >= buf.size) {
                    bufOffset = 0

                    for (i in buf.indices)
                        audioData[i] = buf[i].toFloat()

                    try {
                        callback.onDataReceived(audioData)
                    } catch (e: InterruptedException) {
                        Log.e("FFT", e.toString())
                    } catch (e: Exception) {
                        Log.e("FFT", e.toString())
                    }
                }
            }
        } catch (e: InterruptedException) {
            Log.e("FFT", e.toString())
        } finally {
            audioRec.stop()
            audioRec.release()
        }
    }

//    private fun compute(buf: ShortArray) {
////        for (i in 0 until bufSize)
////            dataBuf[i] = buf[i].toFloat()
//
//        callback.onWaveFormDataCapture(buf, -1)
//    }
//
//    private fun fillComplexBuf(buf: ByteArray) {
//        for (i in 0 until bufSize) {
//            complexBuf[2*i] = buf[i].toFloat()
//            complexBuf[2*i+1] = 0.0f
//        }
//    }
//
//    private fun fillDataBuf() {
//        for (i in 0 until bufSize) {
//            dataBuf[i] = complexBuf[2*i]
//        }
//    }
}
