package sk.sintez.fft.module

import kotlin.system.measureTimeMillis

interface IPerfMetrics {
    fun prepare(ids: Iterable<String>)

    fun tick(measureId: String): Long
    fun measure(measureId: String, block: () -> Unit): Long

    fun getMetrics(): Map<String, Long>

}

class PerfMetricsImpl(val measureHistory: Int) : IPerfMetrics {

    private class IndexArrayPair(var index: Int, val arr: LongArray) {
        constructor(len: Int) : this(0, LongArray(len, { 0 }))

        fun get() = arr[index]

        fun push(value: Long) {
            arr[index] = value
            index = (index + 1) % arr.size
        }

        fun mean() = arr.sum() / arr.size

        fun meanDiff(): Long {
            var v = 0L

            for (i in 0 until arr.size-1) {
                val t0 = arr[(index + i) % arr.size]
                val t1 = arr[(index + i + 1) % arr.size]

                v += t1 - t0
            }

            return v / (arr.size - 1)
        }
    }


    private val metrics = HashMap<String, Long>()
    private val measureMap = HashMap<String, IndexArrayPair>()

    override fun prepare(ids: Iterable<String>) {
        for (measureId in ids) {
            measureMap[measureId] = IndexArrayPair(measureHistory)
            metrics[measureId] = 0
        }
    }

    override fun getMetrics(): Map<String, Long> = metrics

    override fun tick(measureId: String): Long {
        val iap = measureMap[measureId]!!

        val t = System.currentTimeMillis()
        iap.push(t)

        val mean = iap.meanDiff()
        metrics[measureId] = mean

        return mean
    }

    override fun measure(measureId: String, block: () -> Unit): Long {
        val iap = measureMap[measureId]!!

        val tdiff = measureTimeMillis(block)
        iap.push(tdiff)

        val mean = iap.mean()
        metrics[measureId] = mean

        return mean
    }
}

class PerfMetricsFormatter(var header: String? = null) {
    private val sb = StringBuilder()

    fun format(metrics: IPerfMetrics): String {
        sb.clear()

        if (header != null)
            sb.appendLine(header)

        for (e in metrics.getMetrics()) {
            sb.appendLine("${e.key}: ${e.value}ms")
        }

        return sb.toString()
    }
}